#Film Name corrector

Assume there are two video files `神探狄仁杰第1集.MKV` and `godfather.RMVB`
Running the script, it will access the IMDB and 豆瓣電影 to try to correct the name.



original

![](1.PNG)



automatically open IMDB , input the file name according to the rule of regex

![](6.PNG)



List out the options for the above list of web page

![](2.PNG)



for example input 0 for the first option

print out the title, director and full title of the film



![](3.PNG)



Change the title automatically

![](8.PNG)

Iterate for the second file 狄仁杰

![](4.PNG)



this time no page found on IMDB, so jump to douban. continue the process again

![](7.PNG)