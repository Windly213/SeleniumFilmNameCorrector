import os
import re

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

supported_type = ['MP4', 'TS', 'AVI', 'RMVB', 'MKV']
match_rule = '(第\d{1,4}(集|部))|(s\d{1,3}\d{1,3})'
searchEngineList = ['https://www.imdb.com/', 'https://movie.douban.com/']
original_path = ''


def getTitleEpisode(file_name):
    tmp = [];
    tmp = re.compile(match_rule).split(file_name)
    tmp.append("")
    return tmp


def enquiry(filmKeywords, filmEpisode):
    driver = webdriver.Chrome(
        'D:\\baidu(c)\\chromedriver_win32\\chromedriver.exe')
    for i in searchEngineList:
        driver.get(i)
        if 'imdb' in i:
            assert 'IMDb' in driver.title
            inputBox = driver.find_element_by_id('navbar-query')
            dropdown = Select(driver.find_element_by_id('quicksearch'));
            dropdown.select_by_visible_text('Titles')
            inputBox.clear()
            inputBox.send_keys(filmKeywords)
            inputBox.send_keys(Keys.RETURN)
            if 'No results found for' in driver.page_source:
                continue
            selected_element = driver.find_elements_by_css_selector('.findResult.odd>.result_text>a')
            for index, e in enumerate(selected_element):
                print("{}: {} \n".format(index, e.text))
            number = int(input('Please input your choice: '))
            selected_element[number].click()
            directorName = driver.find_element_by_css_selector(
                '.credit_summary_item>a').text
            print("director:{}".format(directorName))
            filmTitle = driver.find_element_by_css_selector(".title_wrapper h1").text
            print("film title: {}".format(filmTitle))
            if filmEpisode is None or filmEpisode == "":
                title = filmTitle + '-' + directorName
            else:
                title = filmTitle + '-' + directorName + '-' + filmEpisode
            print(title)
            return title
        if 'douban' in i:
            assert '豆瓣' in driver.title
            inputBox = driver.find_element_by_css_selector('#inp-query')
            inputBox.clear()
            inputBox.send_keys(filmKeywords)
            inputBox.send_keys(Keys.RETURN)
            if '没有找到关于' in driver.page_source:
                return ""
            selected_element = driver.find_elements_by_css_selector('.title-text')
            for index, e in enumerate(selected_element):
                print("{}: {} \n".format(index, e.text))
            number = int(input('Please input your choice: '))
            selected_element[number].click()
            directorName = driver.find_element_by_css_selector(
                'a[rel="v:directedBy"]').text
            print("director:{}".format(directorName))
            filmTitle = driver.find_element_by_css_selector('span[property="v:itemreviewed"]').text
            print("film title: {}".format(filmTitle))
            filmYear = driver.find_element_by_css_selector("span.year").text
            print("film year: " + filmYear)
            if filmEpisode is None or filmEpisode == "":
                title = filmTitle + filmYear + '-' + directorName
            else:
                title = filmTitle + filmYear + '-' + directorName + '-' + filmEpisode
            print(title)
            return title


def changeName(file, title):
    os.rename(file, title)


def finder(path):
    result = []
    for i in os.listdir(path):
        if os.path.isdir(i):
            result += finder(os.path.join(path, i))
        elif i.endswith(tuple(n for n in supported_type)):
            result.append(os.path.join(path, i))
    return result


path = os.getcwd()

for i in finder(path):
    file, extension = os.path.splitext(i)
    filename = file.split('\\')[-1]
    titleEpisode = getTitleEpisode(filename)
    print(titleEpisode[0] + '-' + titleEpisode[1])
    print("{} {} {}".format(i, filename, extension))
    correctedTitle = enquiry(titleEpisode[0], titleEpisode[1])
    if correctedTitle == "" or correctedTitle is None:
        print("No result")
        exit(1)
    else:
        changeName(i, correctedTitle + extension)
